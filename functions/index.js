const loadFunctions = require('firebase-function-tools')
const functions = require('firebase-functions')
const admin = require('firebase-admin')

exports.simulateProcess = require('./simulateProcess').simulateProcess
exports.rechargeBatteries = require('./simulateProcess').rechargeBatteries

const config = functions.config().firebase

admin.initializeApp(config)

loadFunctions(__dirname, exports)
