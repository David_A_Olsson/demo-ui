const functions = require('firebase-functions');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');

const droneTypes = require('./dronesCore').droneTypes //['treeInspector', 'forestMap']


exports.rechargeBatteries = functions.https.onCall((data, context) => {
  return performOperations(data, context, 'rechargeBatteries')
})

exports.simulateProcess = functions.https.onCall((data, context) => {
  return performOperations(data, context, 'workProgress')
});

// https://firebase.google.com/docs/reference/functions/providers_database_

//cannot use pure http function since we need the firebase auth integration for db access
//exports.simulateProcess = functions.https.onRequest((req, res) => {

// .. instead, use "callable"
//https://firebase.google.com/docs/functions/callable
const performOperations = function(data, context, operation){
  const uid = context.auth.uid;
  
  const users = admin.database().ref(`/users/`)

  let promises = []
  let tranCount = 0
  let typeCount = droneTypes.length

  /***
   * NB Critical to use "once" on note "on" since if using "on" we will end up in a 
   * eternal recursive call situation as data gets updated.
   * 
   ** Unhandled error RangeError: Maximum call stack size exceeded
    at baseKeys (/srv/node_modules/lodash/lodash.js:3482:22)
    at keys (/srv/node_modules/lodash/lodash.js:13307:60)
   */
  return users.once("value").then((uSnapshot) => {
    const users = uSnapshot.val() // will be the entire user collection

    Object.keys(users).forEach(userKey => {
      const user = users[userKey]
      if(!user.drones) return
      // console.log(`Performing drome simulation for user ${JSON.stringify(user.uid)}`)
      droneTypes.forEach(droneType => {
        if(!user.drones[droneType]) return 

        /*const dronesRef = admin.database().ref(`/users/${userKey}/drones/${droneType}`)
        dronesRef.on("value", (dSnapshot) => {
          if(!dSnapshot.exists) return
          const drones = dSnapshot.val()*/
        
          const drones = user.drones[droneType]

          Object.keys(drones).forEach(droneKey => {

            const getHitCount = function(){
              tranCount++;
              let hitCount = drones[droneKey].hitCount?drones[droneKey].hitCount:0;
              if(getStatus()==='Idle' || getStatus()==='Discharged!') return hitCount;
              return hitCount + Math.round(Math.random()*10);
            }  
            const getBatteryLevel = function(discharge){
              tranCount++;
              let batteryLevel = drones[droneKey].batteryLevel;
              if(batteryLevel == null) return 90;
              if(discharge){
                if(batteryLevel === 0) return 0;
                return batteryLevel-1;
              }else{
                if(batteryLevel <= 75) return batteryLevel + 25;
                return 100;
              }              
            }  
            const getStatus = function(){
              tranCount++;
              let status = drones[droneKey].status
              if(!status) return 'Deploying ...';
              if(drones[droneKey].batteryLevel === 0) return 'Discharged!';
              if(drones[droneKey].batteryLevel < 15) return 'Idle';
              return 'Working';
            }
            if(operation === 'rechargeBatteries'){
              promises.push(
                admin.database().ref(`/users/${userKey}/drones/${droneType}/${droneKey}`).update({
                  batteryLevel: getBatteryLevel(false)
                })
              )
            }else{
              promises.push(
                admin.database().ref(`/users/${userKey}/drones/${droneType}/${droneKey}`).update({
                  hitCount: getHitCount(),
                  status: getStatus(),
                  batteryLevel: getBatteryLevel(true)
                })
              )
            }

            /*
            const errorHandler = function(error, committed, snapshot) {
              if (error) {
                // console.log('Transaction failed abnormally!', error);
              } else if (!committed) {
                // console.log('We aborted the transaction (because ada already exists).');
              } else {
                // console.log('Data set', snapshot.val());
              }
            }
            // https://firebase.google.com/docs/reference/node/firebase.database.Reference#transaction
            promises.push(
            admin.database().ref(`/users/${userKey}/drones/${droneType}/${droneKey}/hitCount`).transaction(hitCount => {
              tranCount++;
              // console.log(`Increasing count fror ${JSON.stringify(droneKey)}, curr count ${hitCount}`)
              if(hitCount === null) return 0;
              return hitCount+1;
            }, errorHandler))
            promises.push(
            admin.database().ref(`/users/${userKey}/drones/${droneType}/${droneKey}/status`).transaction(status => {
              // console.log(`Setting status fror ${JSON.stringify(droneKey)}, curr status ${status}, battery at ${drones[droneKey].batteryLevel}`)
              tranCount++;
              if(status === null) return 'Deploying ...';
              if(drones[droneKey].batteryLevel && drones[droneKey].batteryLevel === 0) return 'Discharged!';
              return 'Working';
            }, errorHandler))
            promises.push(
            admin.database().ref(`/users/${userKey}/drones/${droneType}/${droneKey}/batteryLevel`).transaction(batteryLevel => {
              // console.log(`Decreasing battery level for ${JSON.stringify(droneKey)}, curr level is ${batteryLevel}`)
              tranCount++;
              if(batteryLevel === null) return 90;
              if(batteryLevel <= 0) return 100;
              
               // NB1 !!! Do not EvEr change/set variable batteryLevel - this will raise a reqursive call chain resulting in:
               // Unhandled error RangeError: Maximum call stack size exceeded
               // at baseGetTag (/srv/node_modules/lodash/lodash.js:3069:11)
               // at Function.isBoolean (/srv/node_modules/lodash/lodash.js:11384:33)
               // NB 2 !!! Do not return 0 - this will be seen as an "abort transaction" instruction..??
               
              return batteryLevel-1;
            }, errorHandler))
            */
          })
        
      })
    })

    console.log(`Performing drone updates for ${typeCount} type of drones. Started ${promises.length} transactions, perfomed ${tranCount} updates to drones`)

    // return Promise.all(promises)
    Promise.all(promises).then(res => {
      console.log(`All transactions completed by ${res}`)
    })

    return true
  })
};
