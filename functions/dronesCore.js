
const droneNameMap = {
  treeInspector: "Tree quality inspector",
  forestMap: "Top view tree counter",
  hogHunter: "Wild hog hunter"
}

exports.droneTypes = ['treeInspector', 'forestMap', 'hogHunter']

exports.nameMap = function(droneKey){
  return droneNameMap[droneKey]
}
