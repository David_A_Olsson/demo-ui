import React, { Component } from 'react'
import { Activity } from '../../../rmw-shell'
import Button from '@material-ui/core/Button'
import { withFirebase } from 'firekit-provider'
import { withTheme } from '@material-ui/core/styles'
import firebase from 'firebase'

const interval = 90000

let simTimer = null

const performSimulatedActivity = () =>{
  const func = firebase.functions().httpsCallable('simulateProcess');
  func().then(res => {
    console.log(`Completed simulation step.`)
  }).catch(ex => {
    console.log(`Failed in simulation step`, ex)
  })
}

const performRechargeBattery = () =>{
  const func = firebase.functions().httpsCallable('rechargeBatteries');
  func().then(res => {
    console.log(`Completed recharge step.`)
  }).catch(ex => {
    console.log(`Failed in recharge step`, ex)
  })
}

class Simulation extends Component{

  render() {
    const { match: route } = this.props

    return (
      <Activity title={"Simulation control board"}>
        
        <Button variant="contained" color="primary" onClick={performSimulatedActivity} style={{margin:'1em'}}>
          Run sim step!
        </Button>

        <Button variant="contained" color="primary" onClick={performRechargeBattery} style={{margin:'1em'}}>
          Recharge batteries
        </Button>

      </Activity>
    )

  }
  
  componentDidMount() {
    console.log('Simulation mounted...')
    simTimer = setInterval(performSimulatedActivity, interval)
  }

  componentWillUnmount() {
    clearInterval(simTimer)
  }

}

export default withTheme(withFirebase(Simulation))
