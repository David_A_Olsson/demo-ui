import Button from '@material-ui/core/Button'
import CountUp from 'react-countup'
import Group from '@material-ui/icons/Group'
import React, { Component } from 'react'
import { Activity } from '../../rmw-shell'
import { GitHubIcon } from '../../rmw-shell/components/Icons'
import { Line, Bar, Doughnut } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { withFirebase } from 'firekit-provider'
import { withTheme } from '@material-ui/core/styles'
import Scrollbar from '../../rmw-shell/components/Scrollbar'

import AltIconAvatar from '../../rmw-shell/components/AltIconAvatar'
import DroneListIconDefault from '@material-ui/icons/SignalWifi1BarSharp'
import Divider from '@material-ui/core/Divider'
import ReactList from '../../components/ReactMultiSourceList'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

import firebase from 'firebase'

import { droneTypes, nameMap } from '../../config/drones'
import { watchList } from 'firekit/lib/store/lists/actions'

const activeUser = () => {
  try {
    const key = Object.keys(localStorage).find(e => e.match(/persist:root/))
    const data = JSON.parse(localStorage.getItem(key))
    const auth = JSON.parse(data.auth)

    return auth
  } catch (ex) {
    return {fail: ex.message}
  }
}

let authorisedAs = activeUser();

const pathDrones = `users/${authorisedAs.uid}/drones`

class Dashboard extends Component {
  componentDidMount() {
    const { watchList } = this.props
    /*
    const auth = firebase.auth()
    if(auth.currenUser){
      console.log('Firebase user now set')
      authorisedAs = auth.currenUser;
    }*/

    /*
    NB MUST be watchList! watchPath will dispatch trigger on data change, but the state will 
    not replace the array data that the payload then would contain.
    */
    //watchList(pathDrones)
    droneTypes.forEach(droneType => {
      watchList(`${pathDrones}/${droneType}`)
    })
    //watchPath('drones_count')
  }

  componentWillUnmount() {
    const { unwatchList } = this.props
    //unwatchList(pathDrones)
    droneTypes.forEach(droneType => {
      unwatchList(`${pathDrones}/${droneType}`)
    })
  }

  render() {
    const { theme, intl, drones, treeKinds, match: route } = this.props

    const uid = route.params.uid 

    let daysLabels = []
    let daysData = []

    // Aggregate graph data
    if (drones) {
      Object.keys(drones)
        .sort()
        .map(key => {
          daysLabels.push(nameMap(key))
          daysData.push(Object.keys(drones[key]).reduce((acc, droneKey) => {
            acc += drones[key][droneKey].hitCount
            return acc
          }, 0))
        })
    }

    const daysComponentData = {
      labels: daysLabels,
      datasets: [
        {
          label: "Hit count per drone type",
          fill: false,
          lineTension: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: theme.palette.secondary.main,
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: theme.palette.primary.main,
          pointHoverBorderColor: theme.palette.secondary.main,
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: daysData
        }
      ]
    }

    let treeKindsData = []
    let treeKindsLabels = []
    let treeKindsBackgrounColors = []

    if (treeKinds) {
      Object.keys(treeKinds)
        .sort()
        .map(key => {
          treeKindsLabels.push(intl.formatMessage({ id: key }))
          treeKindsBackgrounColors.push(intl.formatMessage({ id: `${key}_color` }))
          treeKindsData.push(treeKinds[key])
          return key
        })
    }

    const treeKindsComponentData = {
      labels: treeKindsLabels,
      datasets: [
        {
          data: treeKindsData,
          backgroundColor: treeKindsBackgrounColors,
          hoverBackgroundColor: treeKindsBackgrounColors
        }
      ]
    }

    const filterFields = [
      { name: 'name' },
      { name: 'description' },
    ]

    const renderDroneItem = (key, val) => {
      const { history } = this.props
  
      const { name = '---', description = '---', status = 'Unknown', 
        droneType = 'Unsupported', droneTypeName = '',
        hitCount = 0, batteryLevel = 75 } = val

      const position = JSON.stringify({ long: 12, lat: 23 })
      const forestName = 'Unknown'
  
      return (
        <div key={key}>
          <ListItem
            onClick={() => history.push(`/users/${authorisedAs.uid}/drones/${droneType}/${key}`)}
            key={key}
          >
            <AltIconAvatar alt="droneType" src={val.photoURL} icon={<DroneListIconDefault />} />
            <ListItemText
              primary={name}
              secondary={description}
              style={{ maxWidth: 250 }}
            />
            <ListItemText
              primary="Drone type"
              secondary={droneTypeName}
              style={{ maxWidth: 150 }}
            />
            <ListItemText
              primary="Status"
              secondary={status}
              style={{ maxWidth: 150 }}
            />
            <ListItemText
              primary="Position"
              secondary={position}
              style={{ maxWidth: 150 }}
            />
            <ListItemText
              primary="Hit count"
              secondary={hitCount}
              style={{ maxWidth: 150 }}
            />
            <ListItemText
              primary="Battery level"
              secondary={batteryLevel}
              style={{ maxWidth: 150 }}
            />
          </ListItem>
          <Divider variant="inset" />
        </div>
      )
    }

    return (
      <Activity
        iconElementRight={
          <Button
            style={{ marginTop: 4 }}
            href="https://github.com/TarikHuber/react-most-wanted"
            target="_blank"
            rel="noopener"
            secondary
            icon={<GitHubIcon />}
          />
        }
        pageTitle={intl.formatMessage({ id: 'dashboard' })}
      >

        <Scrollbar>
          <div
            style={{
              margin: 5,
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              marginTop: 50
            }}
          >
            <div style={{ flexGrow: 1, flexShrink: 1, maxWidth: 600 }}>
              <Bar
                options={{
                  maintainAspectRatio: true
                }}
                data={daysComponentData}
              />
            </div>
            <div style={{ flexGrow: 1, flexShrink: 1, maxWidth: 600, justifyContent: 'center' }}>
              <Doughnut data={treeKindsComponentData} />
            </div>
          </div>

          <ReactList
            path={pathDrones}
            filterFields={filterFields}
            renderItem={renderDroneItem}
            subLevels={droneTypes}
          />

        </Scrollbar>
      </Activity>
    )
  }
}

/*

            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', margin: 30 }}>
              <CountUp
                style={{
                  fontSize: 100,
                  color: theme.palette.primary.main,
                  fontFamily: theme.fontFamily
                }}
                start={0}
                end={dronesCount}
              />
              <div>
                <Group color="secondary" className="material-icons" style={{ fontSize: 70, marginLeft: 16 }} />
              </div>
            </div>
            */

const mapStateToProps = state => {
  const { paths } = state

  const stateProps = {
    drones: paths[pathDrones],
    //dronesCount: paths['drones_count'] ? paths['drones_count'] : 0
  }
  
  droneTypes.forEach(droneType => {
    stateProps[droneType] = `users/${authorisedAs.uid}/drones/${droneType}`
  })

  return stateProps
}

export default connect(mapStateToProps)(injectIntl(withTheme(withFirebase(Dashboard))))
