import React, { Component } from 'react'
import { withRouter } from "react-router";
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { withFirebase } from 'firekit-provider'
import { withTheme, withStyles } from '@material-ui/core/styles'
//import { Activity } from '../../rmw-shell'

import EditActivity from '../../rmw-shell/containers/Activities/EditActivity'
import DataInput from "../../components/Forms/Drone"

import firebase from 'firebase'

import { droneTypes, nameMap } from '../../config/drones'

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    margin: '1em'
  },
  root: {
    flexGrow: 1,
    flex: '1 0 100%',
  }
})


let authorisedAs = null

const auth = firebase.auth();
const activeUser = () => {
  if(auth.currentUser !== null){
    return auth.currentUser;
  }
  console.log('firebase auth not ready? Falling back to manuall resolve...')
  try {
    const key = Object.keys(localStorage).find(e => e.match(/persist:root/))
    const data = JSON.parse(localStorage.getItem(key))
    const auth = JSON.parse(data.auth)

    return auth
  } catch (ex) {
    return {fail: ex.message}
  }
}
authorisedAs = activeUser();

class Dashboard extends Component {

  componentDidMount() {

  }

  /*
      <Activity title={intl.formatMessage({ id: 'drone_new' })}>
      <div className={classes.main}>
        <h3 style={{textAlign:'center'}}>Enter name and optional description of new drone</h3>
        <EditActivity
            name={droneType}
            path={dataPath}
            fireFormProps={{
              validate
            }}
          >
          <DataInput droneType {...this.props} />
        </EditActivity>
      </div>
    </Activity>
    */

  render() {
    const { classes, intl, match, location, history } = this.props
    //const droneType = location.pathname.substr(location.pathname.lastIndexOf('/')+1);
    const droneType = match.params.droneType // use defined route param
    const droneId = match.params.uid // given only if editing
    // ... but do NOT append drone is to path "${droneId}" .. this is done internally 
    // by rmw-shell/src/containers/Activities/EditActivity.js using the :uid part of route
    const dataPath = `users/${authorisedAs.uid}/drones/${droneType}`
    // console.log(`Editor drone by path "${dataPath}"`)

    const validate = values => {
      const errors = {}
  
      /*errors.name = !values.name ? intl.formatMessage({ id: 'error_required_field' }) : ''
      errors.full_name = !values.full_name ? intl.formatMessage({ id: 'error_required_field' }) : ''
      errors.vat = !values.vat ? intl.formatMessage({ id: 'error_required_field' }) : ''
      */
      return errors
    }
    return (
        <EditActivity
            header={droneId?
              `Edit drone ${nameMap(droneType)} (id ${droneId})`:
              `Deploy new ${nameMap(droneType)} drone`}
            name={"droneForm"}
            path={dataPath}
            fireFormProps={{
              validate
            }}
          >
          <DataInput {...this.props} />
        </EditActivity>
    )
  }
}

const mapStateToProps = state => {
  const { paths } = state

  return {
  }
}

export default connect(mapStateToProps)(injectIntl(withFirebase(withRouter(withStyles(styles, { withTheme: true })(Dashboard)))))