import drone1 from '../images/TreeScanner.png'
import drone2 from '../images/HogHunter.png'
import drone3 from '../images/MapScanner.png'

import { droneTypes, nameMap } from './dronesCore'

const droneImageMap = {
  treeInspector: drone1,
  forestMap: drone3,
  hogHunter: drone2
}

const imageMap = function(droneKey){
  return droneImageMap[droneKey]
}

export {droneTypes, nameMap, imageMap}
