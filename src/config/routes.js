/* eslint-disable react/jsx-key */
import React from 'react'
import RestrictedRoute from '../rmw-shell/containers/RestrictedRoute'
import makeLoadable from '../rmw-shell/containers/MyLoadable'

const MyLoadable = (opts, preloadComponents) =>
  makeLoadable({ ...opts, firebase: () => import('./firebase') }, preloadComponents)

const AsyncDeployDrone = MyLoadable({ loader: () => import('../pages/DeployDrone') })
const AsyncDashboard = MyLoadable({ loader: () => import('../pages/Dashboard') }, [AsyncDeployDrone])

const AsyncCompany = MyLoadable({ loader: () => import('../pages/Companies/Company') })
const AsyncCompanies = MyLoadable({ loader: () => import('../pages/Companies/Companies') }, [AsyncCompany])

const AsyncAdminSimulation = MyLoadable({ loader: () => import('../pages/Admin/Simulation') })

/*
const AsyncAbout = MyLoadable({ loader: () => import('../pages/About') })
const AsyncTask = MyLoadable({ loader: () => import('../pages/Tasks/Task') })
const AsyncTasks = MyLoadable({ loader: () => import('../pages/Tasks/Tasks') }, [AsyncTask])
*/

const routes = [
  <RestrictedRoute type="private" path="/" exact component={AsyncDashboard} />,
  <RestrictedRoute type="private" path="/dashboard" exact component={AsyncDashboard} />,
  <RestrictedRoute type="private" path="/admin/simulation" exact component={AsyncAdminSimulation} />,
  <RestrictedRoute type="private" path="/users/:uid/drones" exact component={AsyncDashboard} />,
  <RestrictedRoute type="private" path="/users/:uid/drones/:droneType" exact component={AsyncDashboard} />,
  // NB the ":uid" shall always be the critial id since it's appended to given db path 
  // internally by rmw-shell/src/containers/Activities/EditActivity.js
  <RestrictedRoute type="private" path="/users/:userId/drones/:droneType/:uid" exact component={AsyncDeployDrone} />,
  <RestrictedRoute type="private" path="/new_drone/:droneType" exact component={AsyncDeployDrone} />
  
  ,<RestrictedRoute type="private" path="/companies" exact component={AsyncCompanies} />,
  <RestrictedRoute type="private" path="/companies/edit/:uid" exact component={AsyncCompany} />,
  <RestrictedRoute type="private" path="/companies/create" exact component={AsyncCompany} />,

  /*  <RestrictedRoute type="private" path="/about" exact component={AsyncAbout} />
  <RestrictedRoute type="private" path="/tasks" exact component={AsyncTasks} />,
  <RestrictedRoute type="private" path="/tasks/create" exact component={AsyncTask} />,
  <RestrictedRoute type="private" path="/tasks/edit/:uid" exact component={AsyncTask} />*/
]

export default routes
