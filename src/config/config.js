import React from 'react'
import Loadable from 'react-loadable'
import getMenuItems from './menuItems'
import LoadingComponent from '../rmw-shell/components/LoadingComponent'
import locales from './locales'
import routes from './routes'
import themes from './themes'
import grants from './grants'
import parseLanguages from '../rmw-shell/utils/localeTools'

const Loading = () => <LoadingComponent />

const LPAsync = Loadable({
  loader: () => import('../../src/pages/LandingPage'),
  loading: Loading,
})

const config = {
  firebase_config: {
    apiKey: "AIzaSyC70yu-_2AY5ZrXkhXa1-XqwAenjIV3Yaw",
    authDomain: "katam-demo.firebaseapp.com",
    databaseURL: "https://katam-demo.firebaseio.com",
    projectId: "katam-demo",
    storageBucket: "katam-demo.appspot.com",
    messagingSenderId: "819533954236",
    appId: "1:819533954236:web:006420c54019500522e4b9",
    measurementId: "G-LS2B22QB1W"
  },
  firebase_config_dev: {
    apiKey: "AIzaSyC70yu-_2AY5ZrXkhXa1-XqwAenjIV3Yaw",
    authDomain: "katam-demo.firebaseapp.com",
    databaseURL: "https://katam-demo.firebaseio.com",
    projectId: "katam-demo",
    storageBucket: "katam-demo.appspot.com",
    messagingSenderId: "819533954236",
    appId: "1:819533954236:web:006420c54019500522e4b9",
    measurementId: "G-LS2B22QB1W"
  },
  firebase_providers: [
    'google.com',
    'twitter.com',
    /*'facebook.com',    
    'github.com',
    'phone',*/
    'password',
  ],
  googleMaps: {
    apiKey: 'AIzaSyByMSTTLt1Mf_4K1J9necAbw2NPDu2WD7g',
  },
  initial_state: {
    themeSource: {
      isNightModeOn: false,
      source: 'default',
    },
    locale: parseLanguages(['en', 'bs', 'es', 'ru', 'de'], 'en'),
  },
  // see https://github.com/TarikHuber/react-most-wanted/blob/master/packages/rmw-shell/src/containers/ResponsiveDrawer/ResponsiveDrawer.js
  // (E:\Work-Projects\Softhouse\Katam\Demo\softhouse-katam-rmw\node_modules\rmw-shell\lib\containers\ResponsiveDrawer\ResponsiveDrawer.js)
  // is likely meant to be used there, but it's not ....
  drawerWidth: 356, 
  drawer_width: 356, 
  locales,
  themes,
  grants,
  routes,
  getMenuItems,
  firebaseLoad: () => import('./firebase'),
  landingPage: LPAsync,
}

export default config
