
const droneNameMap = {
  treeInspector: "Tree quality inspector",
  forestMap: "Top view tree counter",
  hogHunter: "Wild hog hunter"
}

const droneTypes = ['treeInspector', 'forestMap', 'hogHunter']

const nameMap = function(droneKey){
  return droneNameMap[droneKey]
}

export {droneTypes, nameMap}