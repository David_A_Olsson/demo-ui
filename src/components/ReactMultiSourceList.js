/*import Activity from 'rmw-shell/lib/containers/Activity'
import Add from '@material-ui/icons/Add'
import FilterList from '@material-ui/icons/FilterList'
import IconButton from '@material-ui/core/IconButton'*/
import List from '@material-ui/core/List'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ReactList from 'react-list'
/*import Scrollbar from 'rmw-shell/lib/components/Scrollbar'
import SearchField from 'rmw-shell/lib/components/SearchField'
import Tooltip from '@material-ui/core/Tooltip'*/
import isGranted from '../rmw-shell/utils/auth'
//import { Fab } from '@material-ui/core'
import {
  FilterDrawer,
  filterSelectors,
  filterActions,
} from 'material-ui-filter'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { getList } from 'firekit'
import { getLocation } from 'firekit/lib/store/lists/actions'
import { injectIntl } from 'react-intl'
import { withFirebase } from 'firekit-provider'
import { withRouter } from 'react-router-dom'
//import { ARRAY_TYPE } from 'material-ui-filter/lib/store/selectors'

import { droneTypes, nameMap } from '../config/drones'

class ListActivity extends Component {
  /*
  No need to watch for data changes here 
  (as long as the watchList's are not disabled on Dashboard where this component is used..)
  componentDidMount() {
    const { watchList, path, name } = this.props
    const watchPath = path || name
    //watchList(watchPath)
        
    droneTypes.forEach(subLevel => {
      const sub = watchPath + '/' + subLevel
      console.log(`  watching list ${sub}`)
      watchList(sub)
    })
  }

  componentWillUnmount() {
    const { unwatchList, path, name } = this.props
    const watchPath = path || name
    //unwatchList(watchPath)

    droneTypes.forEach(subLevel => {
      const sub = watchPath + '/' + subLevel
      console.log(`  UN-watching list ${sub}`)
      unwatchList(sub)
    })
  }*/

  render() {
    const {
      createGrant,
      filterFields,
      hasFilters,
      history,
      intl,
      isGranted,
      allDroneList,
      name,
      setFilterIsOpen,
      renderItem,
      handleCreateClick,
      disableCreate,
      title,
      activityProps = {},
      reactListProps = {}
    } = this.props

    const fields = filterFields.map(field => {
      if (!field.label) {
        return {
          label: intl.formatMessage({ id: `${field.name}_label` }),
          ...field,
        }
      }
      return field
    })

    return (
      <List>
        <ReactList
          itemRenderer={i => renderItem(allDroneList[i].key, allDroneList[i].val)}
          length={allDroneList ? allDroneList.length : 0}
          type="simple"
          {...reactListProps}
        />
      </List>

    )
  }
}

ListActivity.propTypes = {
  isGranted: PropTypes.func.isRequired,
}

/*
mapStateToProps is called every time an action is dispatched which causes the store to change,
hence due to action @@firekit/PATHS@VALUE_CHANGED being raised for every drone update,
mapStateToProps (and render) will be called once for each drone.
*/
const mapStateToProps = (state, ownProps) => {
  const { firebaseApp } = ownProps
  const { filters, lists } = state
  const { name, path, isGranted: customIsGranted } = ownProps
  
  const location = firebaseApp ? getLocation(firebaseApp, path) : path
  const ref = location || name // db path reference

  const { hasFilters } = filterSelectors.selectFilterProps(name, filters)
  let newList = []

  //console.log(`Current slist in redux: ${JSON.stringify(lists)}`)
  
  /**
   * If sub levels are defined, these must be used with filterSelectors.getFilteredList(),
   * or no add/remove changes will be reflected in list (changes withing each element will however..)
   */
  droneTypes.forEach(sub => {
    const subRef = ref + '/' + sub
    const tmp = filterSelectors.getFilteredList(
      subRef,
      filters,
      getList(state, subRef),
      fieldValue => fieldValue.val
    )
    tmp.forEach(e => { 
      e.val.droneType = sub
      e.val.droneTypeName = nameMap(sub)
    })
    newList = newList.concat(tmp)
  })
  
  /*const list = listTmp.reduce((acc, droneTypeColl) => {
    const droneType = droneTypeColl.key
    if(!acc) acc = []
    if(!droneTypeColl.val.hasOwnProperty("name")){
      Object.getOwnPropertyNames(droneTypeColl.val)
      .forEach(key => {
        droneTypeColl.val[key].droneType = droneType;
        acc.push({key: key, val: droneTypeColl.val[key]});
      });
    }else{
      acc.push(droneTypeColl)
    }
    return acc
  }, [])
  */
  //console.log(`*** Rendering all ${newList.length} drones, new data: ${JSON.stringify(newList)}`)
  console.log(`*** Rendering all ${newList.length} drones`)

  return {
    ref,
    name,
    hasFilters,
    allDroneList: newList,
    isGranted: grant =>
      customIsGranted ? customIsGranted(state, grant) : isGranted(state, grant),
  }
}

export default compose(
  connect(mapStateToProps, { ...filterActions }),
  injectIntl,
  withFirebase,
  withRouter
)(ListActivity)