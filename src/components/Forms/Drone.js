//import AvatarImageField from 'rmw-shell/lib/components/ReduxFormFields/AvatarImageField'
import Business from '@material-ui/icons/Business'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import TextField from '../../rmw-shell/components/ReduxFormFields/TextField'
//import HiddenReduxField from './HiddenReduxField'
import { Field, reduxForm, formValueSelector } from 'redux-form'
//import { ImageCropDialog } from 'rmw-shell/lib/containers/ImageCropDialog'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { setDialogIsOpen } from '../../rmw-shell/store/dialogs/actions'
import { withRouter } from 'react-router-dom'
import { withTheme } from '@material-ui/core/styles'

import { useField, splitFormProps,  } from "react-form";

import logo from '../../images/ent.png';

import MaterialButton from '@material-ui/core/Button' // 'rmw-shell/lib/components/ReduxFormFields/ActionToggleButton'

import { droneTypes, nameMap, imageMap } from '../../config/drones'

const InputField = React.forwardRef((props, ref) => {
  // Let's use splitFormProps to get form-specific props
  const [field, fieldOptions, rest] = splitFormProps(props);

  // Use the useField hook with a field and field options
  // to access field state
  const {
    meta: { error, isTouched, isValidating },
    getInputProps
  } = useField(field, fieldOptions);

  // Build the field
  return (
    <>
      <input {...getInputProps({ ref, ...rest })} />{" "}
    </>
  );
});

class Form extends Component {
  render() {
    const { handleSubmit, intl, initialized, setDialogIsOpen, dialogs, match } = this.props

    const uid = match.params.userId
    const droneId = match.params.uid
    const droneType = match.params.droneType || 'default'

    return (
      <form
        onSubmit={handleSubmit}
        style={{
          height: '100%',
          width: '100%',
          alignItems: 'strech',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <img src={imageMap(droneType)} className="App-logo" alt="logo" style={{width:'40%'}} />

        <div style={{ margin: 15, display: 'flex', flexDirection: 'column' }}>
          
          <div>
            <Field
              name="name"
              disabled={!initialized}
              component={TextField}
              placeholder={intl.formatMessage({ id: 'name_hint' })}
              label={intl.formatMessage({ id: 'name_label' })}
            />
          </div>

          <div>
            <Field
              name="description"
              disabled={!initialized}
              component={TextField}
              multiline
              rows={1}
              placeholder={intl.formatMessage({ id: 'description_hint' })}
              label={intl.formatMessage({ id: 'description_label' })}
            />
          </div>

          <div style={{display:'none'}}>
            <Field
              name="droneType"
              val={droneType.toString()}
              component={TextField}
              onSubmit={()=>{this.val = droneType.toString()}} />
          </div>

          <div style={{marginTop:'1em'}}>
            {(uid === undefined && (
              <MaterialButton color="primary" variant="contained" type="submit">Deploy new drone</MaterialButton>
            ))}
            {(uid !== undefined && (
              <MaterialButton color="primary" variant="contained" type="submit">Save changes</MaterialButton>
            ))}            
          </div>          

        </div>
      </form>
    )
  }
}

Form.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  initialized: PropTypes.bool.isRequired,
  setDialogIsOpen: PropTypes.func.isRequired,
  dialogs: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
}

/**
 * The reduxForm form stuff relates to editing an existing drone,
 * it needs to math the last path-part, i.e. the "name"-arg given to to <EditActivity ..>
 * "the name of your form and the key to where your form's state will be mounted under the redux-form reducer"
 */
Form = reduxForm({ form: 'droneForm' })(Form)
const selector = formValueSelector('droneForm')

const mapStateToProps = state => {
  const { intl, vehicleTypes, drones, dialogs } = state

  return {
    intl,
    vehicleTypes,
    drones,
    dialogs,
    photoURL: selector(state, 'photoURL')
  }
}

export default connect(
  mapStateToProps,
  { setDialogIsOpen }
)(injectIntl(withRouter(withTheme(Form))))
