import config from '../config'

export const initState = {
  auth: { isAuthorised: false },
  //rootLabels: { headerTitlePrefix: "Drone Controller" },
  ...config.initial_state
}

export default initState
