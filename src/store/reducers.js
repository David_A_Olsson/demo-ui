import { combineReducers } from 'redux'
import initState from './init'
import { appReducers } from '../rmw-shell/store/reducers'
import rootReducer from '../rmw-shell/store/rootReducer'

const appReducer = combineReducers({
  ...appReducers
})

export default (state, action) => rootReducer(appReducer, initState, state, action)
