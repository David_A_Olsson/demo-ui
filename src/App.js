import App from './rmw-shell'
import React from 'react'
import config from './config'
import configureStore from './store'
import { withStyles } from '@material-ui/core/styles'

const drawerWidth = 440;

const styles = {
  drawerPaper: {
    height: '100vh',
    width: drawerWidth,
  },
  drawerPaperOpen: {
    height: '100vh',
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth
  },
}

const Main = () => <App appConfig={{ configureStore, ...config }} />

export default withStyles(styles)(Main)
