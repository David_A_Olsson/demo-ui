
import React from 'react'
//import ReactDOM from 'react-dom'
//import * as serviceWorker from './serviceWorker';
//import UserProvider from "./providers/userProvider";
import * as serviceWorker from './rmw-shell/utils/serviceWorker'
/*
import Loadable from 'react-loadable'
import LoadingComponent from 'rmw-shell/lib/components/LoadingComponent'
*/
/*
const Loading = () => <LoadingComponent />
export const AppAsync = Loadable({
  loader: () => import('./App'),
  loading: Loading,
})
*/
import App from './App'
import ReactDOM from 'react-dom'
/*
React StrictMode is a feature added in version 16.3 and aimed to help us in finding potential problems in an application.
If you see warnings in StrictMode, you will likely encounter bugs when trying to use Concurrent React.
<React.StrictMode>
*/
ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.register({})
