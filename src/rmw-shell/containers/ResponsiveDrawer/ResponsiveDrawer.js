import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import drawerActions from '../../store/drawer/actions'
import withWidth, { isWidthDown } from '@material-ui/core/withWidth'
import { withStyles } from '@material-ui/core/styles'
import { compose } from 'redux'
import { connect } from 'react-redux'

const drawerWidth = 340

const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent)

const styles = theme => ({
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  drawerPaper: {
    height: '100vh',
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperOpen: {
    height: '100vh',
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    height: '100vh',
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(1) * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(1) * 9,
    },
  },

  hide: {
    display: 'none',
  },
})

class ResponsiveDrawer extends React.Component {
  /*state = {
    mobileOpen: false,
    open: false,
  }*/

  handleDrawerToggle = () => {
    const { setDrawerMobileOpen, drawer } = this.props
    setDrawerMobileOpen(!drawer.mobileOpen)
  }

  handleDrawerOpen = () => {
    const { setDrawerOpen } = this.props
    setDrawerOpen(true)
  }

  handleDrawerClose = () => {
    const { setDrawerOpen } = this.props
    setDrawerOpen(false)
  }
  
  componentDidMount = () => {
    // This "auto expand" will do the trick when reloading page or for hot webpack rebuild,
    // but not when logging in - in that case the component has already been mounted 
    // via SignIn page.
    /*const { width, setDrawerOpen, drawer } = this.props
    const smDown = isWidthDown('sm', width)
    if (!smDown && !drawer.open) {
      console.log("  Triggering menu open")
      setTimeout((doOpen) => {doOpen(true)}, 1000, setDrawerOpen)
    }*/
  }

  render() {
    const { classes, theme, children, drawer, width, auth } = this.props

    const smDown = isWidthDown('sm', width)

    //console.log("*** Menu items redrawn ****") auth:"{"isAuthorised":false}"
    
    // isAuthorised check to avoid that menu is auto-exanded on login page
    const { setDrawerOpen } = this.props
    if (auth.isAuthorised && !smDown && !drawer.open && !drawer.manuallyClosed) {
      console.log(`    Triggering menu open from renderer. smDown:${smDown}, open:${drawer.open}, mc:${drawer.manuallyClosed}`)
      // NB Since setDrawerOpen will trigger a new re-render of drawer, 
      // make damn sure that state is changed in a way that to not cause a infinitly loop here ...
      setTimeout((doOpen) => {doOpen(true)}, 750, setDrawerOpen)
    }

    return (
      <div style={{ boxSizing: 'content-box' }}>
        <SwipeableDrawer
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
          variant={smDown ? 'temporary' : 'permanent'}
          onClose={this.handleDrawerToggle}
          anchor={
            smDown ? undefined : theme.direction === 'rtl' ? 'right' : 'left'
          }
          classes={{
            paper: smDown
              ? classes.drawerPaper
              : classNames(
                classes.drawerPaperOpen,
                !drawer.open && classes.drawerPaperClose,
                !drawer.useMinified && classes.hide
              ),
          }}
          open={smDown ? drawer.mobileOpen : drawer.open}
          onOpen={this.handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {children}
        </SwipeableDrawer>
      </div>
    )
  }
}

ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  const { drawer, auth } = state

  return {
    drawer,
    auth
  }
}

export default compose(
  connect(mapStateToProps, { ...drawerActions }),
  withWidth(),
  withStyles(styles, { withTheme: true })
)(ResponsiveDrawer)
