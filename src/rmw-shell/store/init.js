import { isAuthorised } from '../utils/auth'

export const initState = {
  auth: { isAuthorised: isAuthorised() }
  //rootLabels: { headerTitlePrefix: "Drone Controller" },
}

export default initState
