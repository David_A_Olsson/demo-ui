import * as types from './types'

export function setDrawerOpen(open, manuallyClosed) {
  return {
    type: types.ON_DRAWER_OPEN_CHANGED,
    open,
    manuallyClosed
  }
}

export function setDrawerMobileOpen(mobileOpen) {
  return {
    type: types.ON_DRAWER_MOBILE_OPEN_CHANGED,
    mobileOpen
  }
}

export function setDrawerUseMinified(useMinified, manuallyClosed) {
  return {
    type: types.ON_DRAWER_USE_MINIFIED_CHANGED,
    useMinified,
    manuallyClosed
  }
}

export default { setDrawerMobileOpen, setDrawerOpen, setDrawerUseMinified }
